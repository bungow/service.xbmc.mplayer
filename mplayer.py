#! /usr/bin/python

import sys, socket, commands, os

localPort = 9778
remoteHost = 'localhost'
remotePort = 9777

def mplayer(s):
      fifo = open('/tmp/mplayerfifo', 'w+') 
      fifo.write(s + '\n')
      fifo.close()
  
def scanpackage(x):
      if ('5' in x):
            if ('display' in str(x)):
                  mplayer('mute')

            elif ('myvideo' in str(x)):
                  mplayer('stop')

            elif ('mymusic' in str(x)):
                  mplayer('stop')

            elif ('mypictures' in str(x)):
                  mplayer('stop')

            elif ('mytv' in str(x)):
                  mplayer('stop')

            elif ('play' in str(x)):
                  mplayer('pause')

            elif ('stop' in str(x)):
                  mplayer('stop')

            elif ('pause' in str(x)):
                  mplayer('pause')

            elif ('reverse' in str(x)):
                  mplayer('mute')

            elif ('forward' in str(x)):
                  mplayer('seek +150')

            elif ('skipminus' in str(x)):
                  mplayer('seek -150')

            elif ('skipplus' in str(x)):
                  mplayer('stop')

            elif ('up' in str(x)):
                  mplayer('menu up')

            elif ('right' in str(x)):
                  mplayer('menu right')

            elif ('down' in str(x)):
                  mplayer('menu down')

            elif ('left' in str(x)):
                  mplayer('menu left')

            elif ('select' in str(x)):
                  mplayer('menu select')

            elif ('title' in str(x)):
                  pass

            elif ('info' in str(x)):
                  pass

            elif ('menu' in str(x)):
                  pass
                  
            elif ('back' in str(x)):
                  pass

            elif ('power' in str(x)):
                  mplayer('stop')
                  
            elif ('volumeplus' in str(x)):
                  mplayer('volume +10')
                  
            elif ('volumeminus' in str(x)):
                  mplayer('volume -10')

            
      else:
            pass
 
try:
      os.mkfifo('/tmp/mplayerfifo')
except OSError, e:
      pass

try:
      localPort = localPort
      remotePort = remotePort
      s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
      s.bind(('', localPort))
except:
      sys.exit()
      
while True:
      data, addr = s.recvfrom(64)
      scanpackage(data)
      s.sendto(data, (remoteHost, remotePort))
